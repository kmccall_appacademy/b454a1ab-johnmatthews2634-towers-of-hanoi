# Towers of Hanoi
#
# Write a Towers of Hanoi game:
# http://en.wikipedia.org/wiki/Towers_of_hanoi
#
# In a class `TowersOfHanoi`, keep a `towers` instance variable that is an array
# of three arrays. Each subarray should represent a tower. Each tower should
# store integers representing the size of its discs. Expose this instance
# variable with an `attr_reader`.
#
# You'll want a `#play` method. In a loop, prompt the user using puts. Ask what
# pile to select a disc from. The pile should be the index of a tower in your
# `@towers` array. Use gets
# (http://andreacfm.com/2011/06/11/learning-ruby-gets-and-chomp.html) to get an
# answer. Similarly, find out which pile the user wants to move the disc to.
# Next, you'll want to do different things depending on whether or not the move
# is valid. Finally, if they have succeeded in moving all of the discs to
# another pile, they win! The loop should end.
#
# You'll want a `TowersOfHanoi#render` method. Don't spend too much time on
# this, just get it playable.
#
# Think about what other helper methods you might want. Here's a list of all the
# instance methods I had in my TowersOfHanoi class:
# * initialize
# * play
# * render
# * won?
# * valid_move?(from_tower, to_tower)
# * move(from_tower, to_tower)
#
# Make sure that the game works in the console. There are also some specs to
# keep you on the right track:
#
# ```bash
# bundle exec rspec spec/towers_of_hanoi_spec.rb
# ```
#
# Make sure to run bundle install first! The specs assume you've implemented the
# methods named above.


class TowersOfHanoi
  attr_accessor :towers
  def initialize
    @towers = []
    tower_one = (1..3).to_a.reverse
    tower_two = []
    tower_three = []
    @towers << tower_one << tower_two << tower_three
  end

  def play
    count = 0
    name = :play
    TracePoint.trace(:call) do |x|
      count += 1 if x.method_id == name
    end
    min_num_of_moves = 2 ** @towers.max.max - 1
    
    until won?
      current_board
      puts "Which tower would you like to move a disc from?"
      from_tower = (gets.to_i - 1)
      puts "Which tower would you like to move a disc to?"
      to_tower = (gets.to_i - 1)
      if valid_move?(from_tower, to_tower)
        move(from_tower, to_tower)
      else
        puts "Invalid move, please try again."
        next
      end
    end

    puts "You won in #{count} moves! Perfect score is #{min_num_of_moves}!"

  end

  def move(from_tower, to_tower)
    disc = @towers[from_tower].pop
    @towers[to_tower].push(disc)
  end

  def valid_move?(from_tower, to_tower)
    if @towers[from_tower].length == 0
      return false
    elsif @towers[to_tower].length == 0
      return true
    elsif @towers[from_tower][-1] > @towers[to_tower][-1]
      return false
    end
    return true
  end

  def won?
    if @towers[1].length == 3 || @towers[2].length == 3
      true
    else
      false
    end
  end

  def current_board
    @towers.each_with_index do |tower, tower_name|
      puts "Tower #{tower_name + 1} has #{tower} disks"
    end

  end

end

if $PROGRAM_NAME == __FILE__
  TowersOfHanoi.new().play
end
